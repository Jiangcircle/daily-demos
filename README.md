﻿***一些日常小Demo***
# 有好多运动都可以用这里面的运动框架来完成
## 完美运动框架
指的是，这个框架可以实现多个属性同时运动，而非链式运动。
### 获取非行间样式的方法：
```
function getStyle (obj, attr) {   //获取非行间样式
    if (obj.currentStyle) {
        return obj.currentStyle[attr]; //针对ie获取非行间样式
    } else {
        return getComputedStyle (obj, false)[attr];//针对谷歌，火狐获取非行间样式
    }
}
```
>>> 引用
>> 引用
> 引用