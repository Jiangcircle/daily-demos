function createSVGPath(startX, stratY, R, theta, width, color) {
    var path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    var realR = R - width;//因为圆弧有边框的厚度
    var dArr = ["M" + startX, stratY + width, "A" + realR, realR, 0, theta >= 180 ? 1 : 0, 1];//最后一项是确定是逆时针还是顺时针
    var cx = startX,
        cy = stratY + R;//确定圆弧中心位置

    var theta2 = theta % 360;
    theta = theta > 0 && theta2 == 0 ? 359.9 : theta2;//如果角度是360则取角度为359.9，否则还按原来的角度来。

    var alpha = theta / 180 * Math.PI;//将角度值换成弧度值。
    var dx = realR * Math.sin(alpha);//计算终点坐标的x值距原点的距离。
    var dy = realR * Math.cos(alpha);//计算终点坐标的y值距圆点的距离。
    var x = cx + dx,
        y = cy - dy;//计算终点坐标的坐标。

    dArr.push(x.toFixed(2));//向数组里面添加参数“终点坐标的横坐标值”保留两位小数。
    dArr.push(y.toFixed(2));//向数组里面添加参数“终点坐标的纵坐标值”保留两位小数。
    var d = dArr.join(" ");//将dArr数组中的元素用“空格”分隔开，并返回这个数组到d变量中。

    path.setAttribute('d', d);
    path.setAttribute('stroke', color);
    path.setAttribute('stroke-width', width);
    path.setAttribute('fill', 'none');

    return path;
}
window.onload = function () {
    var svg = document.getElementById('svg-01');
    var start = document.getElementById("start");
    var back = document.getElementById("back");
    var path = createSVGPath(50, 50, 10, 0, 2, 'red');
    svg.appendChild(path);
    var timer = null
    function startmove() {
        
        clearInterval(timer);
        var iSpeed = 30;
        timer = setInterval(function () {
            var alpha1 = 30 + iSpeed;
            iSpeed = alpha1;
            if (alpha1 >= 360) {
                clearInterval(timer);
                var path1 = createSVGPath(50, 50, 10, 360, 2, 'red');
                svg.replaceChild(path1,svg.childNodes[1]);
                // svg.appendChild(path);
            }
            else {
                var path1 = createSVGPath(50, 50, 10, alpha1, 2, 'red');
                svg.replaceChild(path1,svg.childNodes[1]);
                path = path1;
                // svg.appendChild(path);
            }
        }, 30);
    }
    function backmove() {
        
        clearInterval(timer);
        var iSpeed = 30;
        var svg = document.getElementById('svg-01');
        
        timer = setInterval(function () {
            var alpha1 = 360 - iSpeed;
            iSpeed += 10;
            if (alpha1 <= 30) {
                clearInterval(timer);
                var path2 = createSVGPath(50, 50, 10, 0, 2, 'red');
                svg.replaceChild(path2,svg.childNodes[1]);
                console.log(svg.childNodes);
                
                // svg.appendChild(path);
            }
            else {
                var path2 = createSVGPath(50, 50, 10, alpha1, 2, 'red');
                svg.replaceChild(path2,svg.childNodes[1]);
                console.log(svg.childNodes);
                // svg.appendChild(path);
            }
        }, 30);
    }
    start.onclick = function() {
        startmove();
    }
    back.onclick = function() {
        backmove();
    }
    function circleGo(obj, target1, R , fn) {
        var child = obj.childNodes;
        clearInterval(obj.timer);
        var iSpeed = 30;
        var r = 0;
        obj.timer = setInterval(function(){
            var alpha1 = 30 + iSpeed;
            iSpeed = alpha1;
            if( r == R ){
                r = R;
            } else {
                r++;
            }
            if(alpha1 >= target1 || r >= R) {
                clearInterval(obj.timer);
                var path1 = createSVGPath(20, 0, R, target1, 1, "red");
                obj.replaceChild(path1, child[1]);
                if(fn) {
                    fn();
                }
            } else {
                var path1 = createSVGPath(20, 0, r, alpha1, 1, 'red');
                obj.replaceChild(path1, child[1]);
                path = path1;
            }
        },30);
    }
}

