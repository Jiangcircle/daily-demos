function getStyle(obj, attr) { //获取非行间样式
    if (obj.currentStyle) {
        return obj.currentStyle[attr]; //针对ie获取非行间样式
    } else {
        return getComputedStyle(obj, false)[attr]; //针对谷歌，火狐获取非行间样式
    }
}

function move(obj, json, tim, num, fn) { //封装函数，可获取非行间样式
    clearInterval(obj.timer);
    obj.timer = setInterval(function() {
        var stop = true; //假设循环要停止
        for (var attr in json) {
            var iCur = 0;
            if (attr == 'opacity') {
                iCur = parseInt(parseFloat(getStyle(obj, attr)) * 100);
            } else {
                iCur = parseInt(getStyle(obj, attr));
            }
            if (num == 1) {
                var iSpeed = (json[attr] - iCur) / 10;
            } else {
                var iSpeed = num;
            }
            if (iSpeed > 0) {
                iSpeed = Math.ceil(iSpeed);
            } else {
                iSpeed = Math.floor(iSpeed);
            }
            if (Math.abs(json[attr] - iCur) > 0) { //如果有一个目标值没有达到就让stop = false。
                stop = false;
            }
            if (attr == 'opacity') {
                obj.style[attr] = (iCur + iSpeed) / 100;
            } else {
                obj.style[attr] = iCur + iSpeed + "px"; //注意这一行中的语法 “  obj.style[attr]  ”；
            }
        }
        //判断是否停止
        if (stop) { //如果都达到目标值了，则stop = true；此时就可以停止了。
            clearInterval(obj.timer);
            if (fn) {
                fn();
            }
        }
    }, tim);
}
window.onload = function() {
    var demo = document.getElementById("demo");
    var demo2 = document.getElementById("demo2");
    var demo3 = document.getElementById("demo3");
    var big = document.getElementById("big");
    var timer = null;
    var iSpeed = 0;
    timer = setInterval(function() {
        iSpeed = iSpeed + 1;
        var deg = 'rotate' + '(' + iSpeed + 'deg' + ')';
        if (360 == iSpeed) {
            iSpeed = 0;
            deg = 'rotate' + '(' + 0 + 'deg' + ')';
            demo.style.webkitTransform = deg;
            demo2.style.webkitTransform = deg;
        } else {
            demo.style.webkitTransform = deg;
            demo2.style.webkitTransform = deg;
        }
    }, 50);
    big.onmouseover = function() {
        move(demo2, { marginTop: 40, marginLeft: 40, width: 220, height: 220 }, 5, 1);
    }
    big.onmouseleave = function() {
        move(demo2, { marginTop: 150, marginLeft: 150, width: 0, height: 0 }, 5, 1);
    }
    demo3.onmouseover = function() {
        move(this, { width: 34, height: 34, marginLeft: -2, marginTop: -2 }, 10, 1);
    }
    demo3.onmouseout = function() {
        move(this, { width: 30, height: 30, marginLeft: 0, marginTop: 0 }, 10, 1);
    }
}