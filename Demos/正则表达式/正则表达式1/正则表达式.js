window.onload = function() {
    var demo1 = document.getElementById("demo1").innerHTML;
    var demo1_butt = document.getElementById("demo1_butt");
    var demo1_result = document.getElementById("demo1_result");
    demo1_butt.onclick = function search() {
        var result = demo1.search(/hello/i);
        demo1_result.innerHTML = result;
    }
    var demo2 = document.getElementById("demo2").innerHTML;
    var demo2_butt = document.getElementById("demo2_butt");
    var demo2_result = document.getElementById("demo2_result");
    demo2_butt.onclick = function search() {
        var result = demo2.replace(/hello/i, "你好");
        var result = result.replace(/world/i, "世界");
        demo2_result.innerHTML = result;
    }
    var demo3 = document.getElementById("demo3").innerHTML;
    var demo3_butt = document.getElementById("demo3_butt");
    var demo3_result = document.getElementById("demo3_result");
    demo3_butt.onclick = function text() {
        var patt = /l/;
        var result = patt.test(demo3);
        demo3_result.innerHTML = result;
    }
    var demo4 = document.getElementById("demo4").innerHTML;
    var demo4_butt = document.getElementById("demo4_butt");
    var demo4_result = document.getElementById("demo4_result");
    demo4_butt.onclick = function text() {
        var patt = /o/;
        var result = patt.exec(demo4);
        demo4_result.innerHTML = result;
    }
}