var timer = null;       //定义的变量timer必须在function startMove的外面
function startMove() {
    clearInterval(timer);
    var iDiv = document.getElementById("iDiv");
    timer = setInterval(function () {
        var iSpeed = 1;
        if (iDiv.offsetLeft >= 600) {
            clearInterval(timer);
        } else {
            iDiv.style.marginLeft = iDiv.offsetLeft + iSpeed + "px";
        }
    }, 30);
}
window.onload = function () {
    var start = document.getElementById("start");
    start.onclick = function () {
        startMove();
    }
}