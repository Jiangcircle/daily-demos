function getStyle(obj, attr) {   //获取非行间样式
    if (obj.currentStyle) {
        return obj.currentStyle[attr]; //针对ie获取非行间样式
    } else {
        return getComputedStyle(obj, false)[attr];//针对谷歌，火狐获取非行间样式
    }
}

function move(obj, json, fn) {         //封装函数，可获取非行间样式
    clearInterval(obj.timer);
    obj.timer = setInterval(function () {
        var stop = true;    //假设循环要停止
        for (var attr in json) {
            var iCur = 0;
            if (attr == 'opacity') {
                iCur = parseInt(parseFloat(getStyle(obj, attr)) * 100);
            } else {
                iCur = parseInt(getStyle(obj, attr));
            }
            var iSpeed = (json[attr] - iCur) / 8;
            if (iSpeed > 0) {
                iSpeed = Math.ceil(iSpeed);
            } else {
                iSpeed = Math.floor(iSpeed);
            }
            if (json[attr] != iCur) {  //如果有一个目标值没有达到就让stop = false。
                stop = false;
            }
            if (attr == 'opacity') {
                obj.style[attr] = (iCur + iSpeed) / 100;
            } else {
                obj.style[attr] = iCur + iSpeed + "px";//注意这一行中的语法 “  obj.style[attr]  ”；
            }
        }
        //判断是否停止
        if (stop) {   //如果都达到目标值了，则stop = true；此时就可以停止了。
            clearInterval(obj.timer);
            if (fn) {
                fn();
            }
        }
    }, 30);
}




window.onload = function () {
    var text = document.getElementById("text1");
    var butt = document.getElementById("butt");
    var div1 = document.getElementById("div1");

    butt.onclick = function() {
        var smalldiv = document.createElement("div");  //新创建的信息
        var smalldivs = div1.getElementsByTagName("div");  //展示框中的所有信息框
        smalldiv.innerHTML = text.value;
        text.value = " ";  //发布完后清空；
        if (smalldivs.length) {
            div1.insertBefore(smalldiv, smalldivs[0]);  //将新的信息框插到最前面。
        } else {
            div1.appendChild(smalldiv);
        }

        var iHeight = smalldiv.offsetHeight;
        smalldiv.style.height = 0;
        move(smalldiv, {height: iHeight}, function (){
            move (smalldiv, {opacity: 100});
        });
    }
}