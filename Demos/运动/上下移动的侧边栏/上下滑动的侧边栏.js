var timer = null;
function move(target) {
    clearInterval(timer);
    var demo = document.getElementById("demo");
    timer = setInterval(function () {
        var iSpeed = (target - demo.offsetTop) / 8;
        if (iSpeed > 0) {
            iSpeed = Math.ceil(iSpeed);
        } else {
            iSpeed = Math.floor(iSpeed);
        }
        if (demo.offsetTop == target) {
            clearInterval(timer);
        } else {
            demo.style.marginTop = demo.offsetTop + iSpeed + "px";
        }
    }, 30)
}
window.onscroll = function () {
    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    var canSeeTop = document.documentElement.clientHeight || document.body.clientHeight;
    var demo = document.getElementById("demo");
    var selfHeight = demo.offsetHeight;
    var targetTop = scrollTop + (canSeeTop - selfHeight) / 2;   //??????
    move(parseInt(targetTop));
    console.log(canSeeTop);
    console.log(selfHeight);
}