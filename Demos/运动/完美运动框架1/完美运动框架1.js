function getStyle (obj, attr) {   //获取非行间样式
    if (obj.currentStyle) {
        return obj.currentStyle[attr]; //针对ie获取非行间样式
    } else {
        return getComputedStyle (obj, false)[attr];//针对谷歌，火狐获取非行间样式
    }
}

function move (obj, json, tim) {         //封装函数，可获取非行间样式
    clearInterval (obj.timer);
    obj.timer =setInterval (function () {
        for (var attr in json) {
            var iCur = 0;
            if (attr == 'opacity') {
                iCur = parseInt (parseFloat ( getStyle(obj, attr) )*100 );
            } else {
                iCur = parseInt( getStyle (obj, attr) );
            }
            var iSpeed = (json[attr] - iCur)/8;
            if (iSpeed > 0) {
                iSpeed = Math.ceil(iSpeed);
            } else {
                iSpeed = Math.floor(iSpeed);
            }
            if (iCur == json[attr]) {
                clearInterval (obj.timer);
            } else {
                if (attr == 'opacity'){
                    obj.style[attr]= (iCur + iSpeed)/100;
                } else {
                    obj.style[attr] = iCur + iSpeed + "px";//注意这一行中的语法 “  obj.style[attr]  ”；
                }
            }
        }
    },tim);
}
window.onload = function () {
    var demo = document.getElementById("demo");
    demo.onclick = function () {
        move (demo, {width: 300, height: 300}, 50);//此运动框架在宽和高设置的大小与原来的比例不一样的时候就会出现问题。
        //move (demo, {width: 300, height: 300}, 50); 比如这个！！
    }
}