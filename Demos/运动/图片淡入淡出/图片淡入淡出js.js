var op = 30;           //变量op是图片透明度的初始值的100倍，并且这个变量要写在function start的外面
var timer = null;
function start(target) {
    clearInterval(timer);
    var images = document.getElementById("images");
    console.log(op);
    timer = setInterval(function () {
        var iSpeed = 0;
        if (op < target){
            iSpeed = 10;
        } else {
            iSpeed = -10;
        }
        if (op == target) {
            clearInterval(timer);
        } else {
            op = op + iSpeed;
            images.style.opacity = op/100;
        }
    }, 40)
}
window.onload = function () {
    var images = document.getElementById("images");
    images.onmouseover = function () {
        start(100);
    }
    images.onmouseout = function () {
        start(30);
    }
}