window.onload = function () {
    var divs = document.getElementsByTagName("div")
    for (i = 0; i < divs.length; i++) {
        divs[i].timer = null;
        divs[i].alpha = 30;     //与多div运动一样，多div淡入淡出也需要各自有一个透明度，避免多个进行干扰。
        divs[i].onmouseover = function () {
            move(this, 100);
        }
        divs[i].onmouseout = function () {
            move(this, 30);
        }
    }
}

function move(obj, target) {
    clearInterval(obj.timer);
    obj.timer = setInterval(function () {
        var iSpeed = (target - obj.alpha) / 8;
        if (iSpeed > 0) {
            iSpeed = Math.ceil(iSpeed);
        } else {
            iSpeed = Math.floor(iSpeed);
        }
        if (obj.alpha == target) {
            clearInterval(obj.timer);
        } else {
            obj.alpha = obj.alpha + iSpeed;
            obj.style.opacity = obj.alpha / 100;
        }
    }, 30);
}