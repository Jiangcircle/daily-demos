var timer = null;
function move (target) {
    var iDiv = document.getElementById("demo");
    clearInterval(timer);
    timer = setInterval(function(){
        var iSpeed = (target - iDiv.offsetLeft)/20  ;
        iSpeed = Math.ceil(iSpeed);
        if(iDiv.offsetLeft >= target){
            clearInterval(timer);
        } else {
            iDiv.style.marginLeft = iDiv.offsetLeft + iSpeed + "px";
        }
    }, 25)
}
window.onload = function(){
    var control = document.getElementById("control");
    control.onclick = function () {
        move(1000)
    }
}