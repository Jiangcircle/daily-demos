function move (obj, target) {
    clearInterval(obj.timer)
    obj.timer = setInterval(function(){
        iSpeed = (target - obj.offsetWidth)/8;
        if (iSpeed > 0){
            iSpeed = Math.ceil(iSpeed);
        } else {
            iSpeed = Math.floor(iSpeed);
        }
        if (obj.offsetWidth == target) {
            clearInterval(obj.timer);
        } else {
            obj.style.width = obj.offsetWidth + iSpeed + "px";
        }
    },30);
}
window.onload = function(){
    var divs = document.getElementsByTagName("div");
    for( i = 0; i < divs.length; i++) {
        divs[i].timer = null;     //这次直接给每一个div一个定时器，这样它们之间就不会互相干扰啦。
        divs[i].onmouseover = function () {
            move(this, 300);
        }
        divs[i].onmouseout = function () {
            move(this, 100);
        }
    }
}