function getStyle (obj, attr) {   //获取非行间样式
    if (obj.currentStyle) {
        return obj.currentStyle[attr]; //针对ie获取非行间样式
    } else {
        return getComputedStyle (obj, false)[attr];//针对谷歌，火狐获取非行间样式
    }
}

function move (obj, attr, target, fn) {         //封装函数，可获取非行间样式
    clearInterval (obj.timer);
    obj.timer =setInterval (function () {
        // var iCur = parseInt( getStyle (obj, attr) );
        var iCur = 0;
        if (attr == 'opacity') {
            iCur = parseInt (parseFloat ( getStyle(obj, attr) )*100 );
        } else {
            iCur = parseInt( getStyle (obj, attr) );
        }
        var iSpeed = (target - iCur)/8;
        if (iSpeed > 0) {
            iSpeed = Math.ceil(iSpeed);
        } else {
            iSpeed = Math.floor(iSpeed);
        }
        if (iCur == target) {
            clearInterval (obj.timer);
            if (fn) {
                fn();
            }
        } else {
            if (attr == 'opacity'){
                obj.style[attr]= (iCur + iSpeed)/100;
            } else {
                obj.style[attr] = iCur + iSpeed + "px";//注意这一行中的语法 “  obj.style[attr]  ”；
            }
        }
    },30);
}
window.onload = function () {
    var demo = document.getElementById ("demo");
    demo.onmouseover = function () {
        move (this, 'width', 300, function () {
            move (demo, 'height', 300);
        });
    }
    demo.onmouseout = function () {
        move (this, 'height', 100, function () {
            move(demo, 'width', 100);
        });
    }
}