function getStyle(obj, attr) {   //获取非行间样式
    if (obj.currentStyle) {
        return obj.currentStyle[attr]; //针对ie获取非行间样式
    } else {
        return getComputedStyle(obj, false)[attr];//针对谷歌，火狐获取非行间样式
    }
}

function move(obj, json, fn) {         //封装函数，可获取非行间样式
    clearInterval(obj.timer);
    obj.timer = setInterval(function () {
        var stop = true;    //假设循环要停止
        for (var attr in json) {
            var iCur = 0;
            if (attr == 'opacity') {
                iCur = parseInt(parseFloat(getStyle(obj, attr)) * 100);
            } else {
                iCur = parseInt(getStyle(obj, attr));
            }
            var iSpeed = (json[attr] - iCur) / 8;
            if (iSpeed > 0) {
                iSpeed = Math.ceil(iSpeed);
            } else {
                iSpeed = Math.floor(iSpeed);
            }
            if (json[attr] != iCur) {  //如果有一个目标值没有达到就让stop = false。
                stop = false;
            }
            if (attr == 'opacity') {
                obj.style[attr] = (iCur + iSpeed) / 100;
            } else {
                obj.style[attr] = iCur + iSpeed + "px";//注意这一行中的语法 “  obj.style[attr]  ”；
            }
        }
        //判断是否停止
        if (stop) {   //如果都达到目标值了，则stop = true；此时就可以停止了。
            clearInterval(obj.timer);
            if (fn) {
                fn();
            }
        }
    }, 30);
}



//********************************************************************************************************/
window.onload = function () {
    var ul1 = document.getElementById ("ul1");
    var lis = ul1.getElementsByTagName("li");
    var miniZindex = 2;
    for (i = 0; i < lis.length; i++) {
        lis[i].style.left = lis[i].offsetLeft + "px";
        lis[i].style.top = lis[i].offsetTop + "px";   //再加定位之前，left，top都是没有用的；
    }
    for (i = 0; i < lis.length; i++) {
        lis[i].style.position = "absolute";
        lis[i].style.margin = "0";
    }
    for (i = 0; i < lis.length; i++){
        lis[i].onmouseover = function () {
            this.style.zIndex = miniZindex++;
            move(this, {width: 200, height: 200, marginLeft: -50, marginTop: -50}, 10);
        }
        lis[i].onmouseout = function () {
            this.style.margin = "0";
            move(this, {width: 100, height: 100}, 10);
        } 
    } 
}