function getStyle (obj, attr) {   //获取非行间样式
    if (obj.currentStyle) {
        return obj.currentStyle[attr]; //针对ie获取非行间样式
    } else {
        return getComputedStyle (obj, false)[attr];//针对谷歌，火狐获取非行间样式
    }
}

function move (obj, attr, target, tim) {         //封装函数，可获取非行间样式
    clearInterval (obj.timer);
    obj.timer =setInterval (function () {
        // var iCur = parseInt( getStyle (obj, attr) );
        var iCur = 0;
        if (attr == 'opacity') {
            iCur = parseInt (parseFloat ( getStyle(obj, attr) )*100 );
        } else {
            iCur = parseInt( getStyle (obj, attr) );
        }
        var iSpeed = (target - iCur)/8;
        if (iSpeed > 0) {
            iSpeed = Math.ceil(iSpeed);
        } else {
            iSpeed = Math.floor(iSpeed);
        }
        if (iCur == target) {
            clearInterval (obj.timer);
        } else {
            if (attr == 'opacity'){
                obj.style[attr]= (iCur + iSpeed)/100;
            } else {
                obj.style[attr] = iCur + iSpeed + "px";//注意这一行中的语法 “  obj.style[attr]  ”；
            }
        }
    },tim);
}

window.onload = function () {
    var divs = document.getElementsByTagName("div");
    divs[0].onclick = function () {
        this.timer = null;
        move (this, 'width', 300, 30);
    }
    divs[1].onclick = function () {
        this.timer = null;
        move (this, 'height', 200, 30);
    }
    divs[2].onclick = function () {
        this.timer = null;
        move (this, 'border-radius', 50, 30);
    }
    divs[3].onclick = function () {
        this.timer = null;
        move (this, 'border-width', 10, 40)
    }
    divs[4].onmouseover = function () {
        this.timer = null;
        move (this, 'opacity', 30, 50);
    }
    divs[4].onmouseout = function () {
        this.timer = null;
        move (this, 'opacity', 100, 60);
    }
}